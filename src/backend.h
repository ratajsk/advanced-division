#pragma once

#include <QObject>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <KLocalizedContext>
#include <KLocalizedString>

class Backend : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString number1 READ number1 WRITE setNumber1 NOTIFY number1Changed)
	Q_PROPERTY(QString number2 READ number2 WRITE setNumber2 NOTIFY number2Changed)
	public:
		explicit Backend(QObject *parent = nullptr);
		QString number1() const;
		void setNumber1(const QString &n1);
		Q_SIGNAL void number1Changed();
		QString number2() const;
		void setNumber2(const QString &n2);
		Q_SIGNAL void number2Changed();

		Q_INVOKABLE QString sendChar(const bool n2, const QChar ch, const int cursorFromEnd);
		Q_INVOKABLE QString update(const bool n2, const QString val);
		Q_INVOKABLE QString calculate(const QString n1, QString n2);
	private:
		QString m_number1 = "";
		QString m_number2 = "";
};
