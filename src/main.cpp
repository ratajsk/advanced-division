#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "backend.h"

int main(int argc, char *argv[]) {
	QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QApplication app(argc, argv);
	KLocalizedString::setApplicationDomain("advdiv");
	QCoreApplication::setApplicationName(QStringLiteral("Advanced Division"));
	
	QQmlApplicationEngine engine;
	
	Backend backend;
	qmlRegisterSingletonInstance<Backend>("cz.ratajs.advdiv", 1, 0, "Backend", &backend);
	
	engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	
	if(engine.rootObjects().isEmpty()) {
		return -1;
	};
	
	return app.exec();
};