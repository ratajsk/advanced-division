import QtQuick 2.6
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

import cz.ratajs.advdiv 1.0

Kirigami.ApplicationWindow {
	id: root

	title: i18nc("@title:window", "Advanced Division")

	FontLoader {
		id: andika
		source: "Andika.ttf"
	}
	FontLoader {
		id: noto
		source: "NotoSans.ttf"
	}

	property var focusedN2: false;


	pageStack.initialPage: Kirigami.Page {
		ColumnLayout {
			id: container

			anchors {
				left: parent.left
				top: parent.top
				right: parent.right
			}
			Layout.alignment: Qt.AlignCenter
			spacing: Kirigami.Units.largeSpacing

			RowLayout {
				id: math

				spacing: Kirigami.Units.smallSpacing

				Controls.TextField {                                                      // [0-9]+(\.[0-9]+((\u0305[0-9])*\u0305)?)?
					id: n1

					focus: true
					KeyNavigation.tab: n2
					font.family: andika.name
					font.pointSize: 3 * Kirigami.Theme.defaultFont.pointSize
					text: Backend.number1

					property string backupText

					Keys.onPressed: {
						switch(event.key) {
							case Qt.Key_Enter:
							case Qt.Key_Return: eq.clicked(); break;
						//case Qt.Key_Left: btnleft.clicked(); break;
						//case Qt.Key_Right: btnfor.clicked(); break;
						//case Qt.Key_Home: btnhome.clicked(); break;
						//case Qt.Key_End: btnend.clicked(); break;
						//case Qt.Key_Tab:
						//case Qt.Key_Backtab:
							case Qt.Key_Slash: event.accepted = true; btntab.clicked(); break;
							case Qt.Key_Backspace: if(text[cursorPosition - 1]=="\u0305" || text.indexOf("\u0305") > -1 && text[cursorPosition - 1]==".") { btnbackspace.clicked(); backupText = text; }; break;
							case Qt.Key_Delete: if(text.indexOf("\u0305") > -1 && text[cursorPosition]==".") { btndel.clicked(); backupText = text; cursorPosition--; }; break;
							case Qt.Key_Clear:
							case Qt.Key_Cancel: btnclear.clicked(); break;
							case Qt.Key_R:
							case Qt.Key_macron:
							case Qt.Key_diaeresis:
							case Qt.Key_Dead_Macron:
							case Qt.Key_Dead_Diaeresis:
							case Qt.Key_Dead_Abovedot: btnrecurring.clicked(); break;
							case Qt.Key_Comma:
						/*case Qt.Key_Period:*/ btndec.clicked(); break;
						//case Qt.Key_0: btn0.clicked(); break;
						//case Qt.Key_1: btn1.clicked(); break;
						//case Qt.Key_2: btn2.clicked(); break;
						//case Qt.Key_3: btn3.clicked(); break;
						//case Qt.Key_4: btn4.clicked(); break;
						//case Qt.Key_5: btn5.clicked(); break;
						//case Qt.Key_6: btn6.clicked(); break;
						//case Qt.Key_7: btn7.clicked(); break;
						//case Qt.Key_8: btn8.clicked(); break;
						//case Qt.Key_9: btn9.clicked(); break;
							default:
								switch(event.text) {
									case "∶":
									case "∕":
									case "÷":
									case "/": event.accepted = true; btntab.clicked(); break;
								};
						};
					}
					onTextChanged: {
						var cursorFromEnd;
						if(text!=Backend.number1) {
							cursorFromEnd = text.length - cursorPosition;
							text = Backend.update(false, text);
							focus = true;
							cursorPosition = text.length - cursorFromEnd;
						};
						if(backupText) {
							cursorFromEnd = text.length - cursorPosition;
							text = backupText;
							backupText = "";
							cursorPosition = text.length - cursorFromEnd;
						};
					}

					onActiveFocusChanged: {
						if(focus) {
							root.focusedN2 = false;
						};
					}
				}
				Controls.Label {                                                          // [0-9]+(\.[0-9]+)?
					Layout.fillWidth: true
					horizontalAlignment: Text.AlignHCenter

					font.pointSize: 3 * Kirigami.Theme.defaultFont.pointSize
					text: "∶"
				}
				Controls.TextField {
					id: n2

					KeyNavigation.tab: n1
					font.family: andika.name
					font.pointSize: 3 * Kirigami.Theme.defaultFont.pointSize
					text: Backend.number2

					property string backupText

					Keys.onPressed: {
						switch(event.key) {
							case Qt.Key_Enter:
							case Qt.Key_Return: eq.clicked(); break;
						//case Qt.Key_Left: btnleft.clicked(); break;
						//case Qt.Key_Right: btnfor.clicked(); break;
						//case Qt.Key_Home: btnhome.clicked(); break;
						//case Qt.Key_End: btnend.clicked(); break;
						//case Qt.Key_Tab:
						//case Qt.Key_Backtab:
							case Qt.Key_Slash: event.accepted = true; btntab.clicked(); break;
							case Qt.Key_Backspace: if(text[cursorPosition - 1]=="\u0305" || text.indexOf("\u0305") > -1 && text[cursorPosition - 1]==".") { btnbackspace.clicked(); backupText = text; }; break;
							case Qt.Key_Delete: if(text.indexOf("\u0305") > -1 && text[cursorPosition]==".") { btndel.clicked(); backupText = text; cursorPosition--; }; break;
							case Qt.Key_Clear:
							case Qt.Key_Cancel: btnclear.clicked(); break;
							case Qt.Key_R:
							case Qt.Key_macron:
							case Qt.Key_diaeresis:
							case Qt.Key_Dead_Macron:
							case Qt.Key_Dead_Diaeresis:
							case Qt.Key_Dead_Abovedot: btnrecurring.clicked(); break;
							case Qt.Key_Comma:
						/*case Qt.Key_Period:*/ btndec.clicked(); break;
						//case Qt.Key_0: btn0.clicked(); break;
						//case Qt.Key_1: btn1.clicked(); break;
						//case Qt.Key_2: btn2.clicked(); break;
						//case Qt.Key_3: btn3.clicked(); break;
						//case Qt.Key_4: btn4.clicked(); break;
						//case Qt.Key_5: btn5.clicked(); break;
						//case Qt.Key_6: btn6.clicked(); break;
						//case Qt.Key_7: btn7.clicked(); break;
						//case Qt.Key_8: btn8.clicked(); break;
						//case Qt.Key_9: btn9.clicked(); break;
							default:
								switch(event.text) {
									case "∶":
									case "∕":
									case "÷":
									case "/": event.accepted = true; btntab.clicked(); break;
								};
						};
					}
					onTextChanged: {
						var cursorFromEnd;
						if(text!=Backend.number2) {
							cursorFromEnd = text.length - cursorPosition;
							text = Backend.update(true, text);
							focus = true;
							cursorPosition = text.length - cursorFromEnd;
						};
						if(backupText) {
							cursorFromEnd = text.length - cursorPosition;
							text = backupText;
							backupText = "";
							cursorPosition = text.length - cursorFromEnd;
						};
					}

					onActiveFocusChanged: {
						if(focus) {
							root.focusedN2 = true;
						};
					}
				}
				Controls.Button {
					id: eq

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "="

					onClicked: {
						result.text = Backend.calculate(n1.text, n2.text);
					}
				}
			}

			Controls.ScrollView {

				Layout.fillWidth: true
				clip: true

				Controls.Label {
					id: result

					Layout.fillWidth: true
					font.family: andika.name
					font.pointSize: 3 * Kirigami.Theme.defaultFont.pointSize
					wrapMode: Text.Wrap
					text: ""
				}

			}

			GridLayout {
				id: keypad

				Layout.fillWidth: true
				Layout.alignment: Qt.AlignCenter
				rowSpacing: 3 * Kirigami.Units.largeSpacing
				columnSpacing: 3 * Kirigami.Units.largeSpacing

				rows: 5
				columns: 4

				Controls.Button {
					id: btn1

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "1"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "1", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "1", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btn2

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "2"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "2", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "2", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btn3

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "3"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "3", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "3", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btnbackspace

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "⌫"

					onClicked: {
						var cursorFromEnd = 0, caretFromEnd = 0;
						if(root.focusedN2) {
							if(n2.text.length < 1)
								return;
							if(n2.cursorPosition && n2.cursorPosition > 0) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
								if(n2.text.slice(n2.text.length - cursorFromEnd).match(/\u0305/)) {
									caretFromEnd = cursorFromEnd - n2.text.slice(n2.text.length - cursorFromEnd).match(/\u0305/g).length;
								}
								else {
									caretFromEnd = cursorFromEnd;
								};
							};
							n2.text = Backend.sendChar(true, "\b", cursorFromEnd);
							n2.focus = true;
							if(caretFromEnd==cursorFromEnd) {
								n2.cursorPosition = n2.text.length - cursorFromEnd;
							}
							else {
								n2.cursorPosition = n2.text.search(new RegExp("(\\.|\\d\\u0305?){"+caretFromEnd+"}$"));
							};
						}
						else {
							if(n1.text.length < 1)
								return;
							if(n1.cursorPosition && n1.cursorPosition > 0) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
								if(n1.text.slice(n1.text.length - cursorFromEnd).match(/\u0305/)) {
									caretFromEnd = cursorFromEnd - n1.text.slice(n1.text.length - cursorFromEnd).match(/\u0305/g).length;
								}
								else {
									caretFromEnd = cursorFromEnd;
								};
							};
							n1.text = Backend.sendChar(false, "\b", cursorFromEnd);
							n1.focus = true;
							if(caretFromEnd==cursorFromEnd) {
								n1.cursorPosition = n1.text.length - cursorFromEnd;
							}
							else {
								n1.cursorPosition = n1.text.search(new RegExp("(\\.|\\d\\u0305?){"+caretFromEnd+"}$"));
							};
						};
					}
				}
				Controls.Button {
					id: btn4

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "4"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "4", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "4", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btn5

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "5"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "5", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "5", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btn6

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "6"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "6", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "6", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btndel

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "⌦"

					onClicked: {
						var cursorFromEnd = 0, cursorPosition;
						if(root.focusedN2) {
							cursorPosition = n2.text.length;
							if(n2.cursorPosition) {
								cursorPosition = n2.cursorPosition;
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							if(cursorFromEnd==0)
								return;
							n2.text = Backend.sendChar(true, "\u007F", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = cursorPosition;
						}
						else {
							cursorPosition = n1.text.length;
							if(n1.cursorPosition) {
								cursorPosition = n1.cursorPosition;
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							if(cursorFromEnd==0)
								return;
							n1.text = Backend.sendChar(false, "\u007F", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = cursorPosition;
						};
					}
				}
				Controls.Button {
					id: btn7

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "7"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "7", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "7", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btn8

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "8"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "8", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "8", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btn9

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "9"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "9", cursorFromEnd);
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "9", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btnclear

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "⌧"

					onClicked: {
						if(root.focusedN2) {
							n2.text = Backend.sendChar(true, "\u0018", 0)
							n2.focus = true;
						}
						else {
							n1.text = Backend.sendChar(false, "\u0018", 0);
							n1.focus = true;
						};
					}
				}
				Controls.Button {
					id: btndec

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "."

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, ".", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, ".", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btn0

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "0"

					onClicked: {
						var cursorFromEnd = 0;
						if(root.focusedN2) {
							if(n2.cursorPosition) {
								cursorFromEnd = n2.text.length - n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "0", cursorFromEnd);
							n2.focus = true;
							n2.cursorPosition = n2.text.length - cursorFromEnd;
						}
						else {
							if(n1.cursorPosition) {
								cursorFromEnd = n1.text.length - n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "0", cursorFromEnd);
							n1.focus = true;
							n1.cursorPosition = n1.text.length - cursorFromEnd;
						};
					}
				}
				Controls.Button {
					id: btnrecurring

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "\u0305"

					onClicked: {
						var cursorPosition;
						if(root.focusedN2) {
							cursorPosition = n2.text.length;
							if(n2.cursorPosition) {
								cursorPosition = n2.cursorPosition;
							};
							n2.text = Backend.sendChar(true, "\u0305", n2.text.length - cursorPosition);
							n2.focus = true;
							n2.cursorPosition = cursorPosition + (n2.text[cursorPosition]=="\u0305" ? 1 : 0);
						}
						else {
							cursorPosition = n1.text.length;
							if(n1.cursorPosition) {
								cursorPosition = n1.cursorPosition;
							};
							n1.text = Backend.sendChar(false, "\u0305", n1.text.length - cursorPosition);
							n1.focus = true;
							n1.cursorPosition = cursorPosition + (n1.text[cursorPosition]=="\u0305" ? 1 : 0);
						};
					}
				}
				Controls.Button {
					id: btnhome

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "\u21a4"

					onClicked: {
						if(root.focusedN2) {
							n2.focus = true;
							n2.cursorPosition = 0;
						}
						else {
							n1.focus = true;
							n1.cursorPosition = 0;
						};
					}
				}
				Controls.Button {
					id: btnleft

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "\u2190"

					onClicked: {
						if(root.focusedN2) {
							n2.focus = true;
							n2.cursorPosition-= n2.text[n2.cursorPosition - 1]=="\u0305" ? 2 : 1;
						}
						else {
							n1.focus = true;
							n1.cursorPosition-= n1.text[n1.cursorPosition - 1]=="\u0305" ? 2 : 1;
						};
					}
				}
				Controls.Button {
					id: btnfor

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "\u2192"

					onClicked: {
						if(root.focusedN2) {
							n2.focus = true;
							n2.cursorPosition+= n2.text[n2.cursorPosition + 1]=="\u0305" ? 2 : 1;
						}
						else {
							n1.focus = true;
							n1.cursorPosition+= n1.text[n1.cursorPosition + 1]=="\u0305" ? 2 : 1;
						};
					}
				}
				Controls.Button {
					id: btntab

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "\u2236"

					onClicked: {
						if(root.focusedN2) {
							n1.focus = true;
						}
						else {
							n2.focus = true;
						};
					}
				}
				Controls.Button {
					id: btnend

					implicitWidth: 5 * Kirigami.Theme.defaultFont.pointSize
					implicitHeight: 5 * Kirigami.Theme.defaultFont.pointSize

					font.family: noto.name
					font.pointSize: 2 * Kirigami.Theme.defaultFont.pointSize
					text: "\u21a6"

					onClicked: {
						if(root.focusedN2) {
							n2.focus = true;
							n2.cursorPosition = n2.text.length;
						}
						else {
							n1.focus = true;
							n1.cursorPosition = n1.text.length;
						};
					}
				}
			}
		}
	}
}
