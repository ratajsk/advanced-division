#include "backend.h"

Backend::Backend(QObject *parent) : QObject(parent) {

};

QString Backend::number1() const {
	return m_number1;
};
QString Backend::number2() const {
	return m_number2;
};

void Backend::setNumber1(const QString &number1) {
	QString n1 = number1;
	n1.replace(",", ".");
	n1.remove(QRegExp("[^0-9\\.\u0305]"));
	if(n1.contains("\u0305")) {
		QString recurring = n1.right(n1.length() - n1.indexOf("\u0305"));
		recurring.replace(QRegExp("([0-9])"), "\\1\u0305");
		n1 = n1.leftRef(n1.indexOf("\u0305"))+recurring;
		if(n1.contains(".")) {
			QString predec = n1.left(n1.indexOf("."));
			predec.remove("\u0305");
			n1 = predec+n1.rightRef(n1.length() - n1.indexOf("."));
		}
		else {
			n1.remove("\u0305");
		};
	};
	n1.replace(QRegExp("\u0305{2,}"), "\u0305");
	n1.replace(QRegExp("(\\..*)\\."), "\\1");
	m_number1 = n1;
	Q_EMIT number1Changed();
};

void Backend::setNumber2(const QString &number2) {
	QString n2 = number2;
	n2.replace(",", ".");
	n2.remove(QRegExp("[^0-9\\.\u0305]"));
	if(n2.contains("\u0305")) {
		QString recurring = n2.right(n2.length() - n2.indexOf("\u0305"));
		recurring.replace(QRegExp("([0-9])"), "\\1\u0305");
		n2 = n2.leftRef(n2.indexOf("\u0305"))+recurring;
		if(n2.contains(".")) {
			QString predec = n2.left(n2.indexOf("."));
			predec.remove("\u0305");
			n2 = predec+n2.rightRef(n2.length() - n2.indexOf("."));
		}
		else {
			n2.remove("\u0305");
		};
	};
	n2.replace(QRegExp("\u0305{2,}"), "\u0305");
	n2.replace(QRegExp("(\\..*)\\."), "\\1");
	m_number2 = n2;
	Q_EMIT number2Changed();
};

Q_INVOKABLE QString Backend::sendChar(const bool n2, const QChar ch, const int cursorFromEnd) {
	if(n2) {
		if(ch=="\b") {
			if(m_number2[m_number2.length() - cursorFromEnd - 1]=="\u0305") {
				setNumber2(m_number2.remove(m_number2.length() - cursorFromEnd - 2, 2));
			}
			else {
				setNumber2(m_number2.remove(m_number2.length() - cursorFromEnd - 1, 1));
			};
		}
		else if(ch=="\u007F") {
			if(m_number2[m_number2.length() - cursorFromEnd + 1]=="\u0305") {
				setNumber2(m_number2.remove(m_number2.length() - cursorFromEnd, 2));
			}
			else {
				setNumber2(m_number2.remove(m_number2.length() - cursorFromEnd, 1));
			};
		}
		else if(ch=="\u0018") {
			setNumber2("");
		}
		else {
			setNumber2(m_number2.insert(m_number2.length() - cursorFromEnd, ch));
		};
		return m_number2;
	};
	if(ch=="\b") {
		if(m_number1[m_number1.length() - cursorFromEnd - 1]=="\u0305") {
			setNumber1(m_number1.remove(m_number1.length() - cursorFromEnd - 2, 2));
		}
		else {
			setNumber1(m_number1.remove(m_number1.length() - cursorFromEnd - 1, 1));
		};
	}
	else if(ch=="\u007F") {
		if(m_number1[m_number1.length() - cursorFromEnd + 1]=="\u0305") {
			setNumber1(m_number1.remove(m_number1.length() - cursorFromEnd, 2));
		}
		else {
			setNumber1(m_number1.remove(m_number1.length() - cursorFromEnd, 1));
		};
	}
	else if(ch=="\u0018") {
		setNumber1("");
	}
	else {
		setNumber1(m_number1.insert(m_number1.length() - cursorFromEnd, ch));
	};
	return m_number1;
};

Q_INVOKABLE QString Backend::update(const bool n2, const QString val) {
	if(n2) {
		setNumber2(val);
		return m_number2;
	}
	setNumber1(val);
	return m_number1;
};

Q_INVOKABLE QString Backend::calculate(const QString n1, const QString n2) {
	setNumber1(n1);
	setNumber2(n2);

	if(m_number1=="" || m_number2=="") {
		return "";
	};
	QProcess p;
	QStringList arg;
	if(m_number1.contains("\u0305")) {
		arg << m_number1.left(m_number1.indexOf("\u0305") - 1)+"["+m_number1.right(m_number1.length() - m_number1.indexOf("\u0305") + 1).remove("\u0305")+"]";
	}
	else {
		arg << m_number1;
	};
	if(m_number2.contains("\u0305")) {
		arg << m_number2.left(m_number2.indexOf("\u0305") - 1)+"["+m_number2.right(m_number2.length() - m_number2.indexOf("\u0305") + 1).remove("\u0305")+"]";
	}
	else {
		arg << m_number2;
	};
	p.start("advdiv", arg);
	p.waitForFinished();
	QString result = p.readAll().replace("\n", "");
	if(result=="Error") {
		result = i18n("Error!");
	}
	if(result.contains("[")) {
		return result.leftRef(result.indexOf("["))+result.right(result.length() - result.indexOf("[") - 1).replace(QRegExp("([0-9])"), "\\1\u0305").replace("]", "");
	}
	else {
		return result;
	}
};
