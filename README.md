Relies on: https://github.com/ratajs/Advanced-Division-c or https://github.com/ratajs/Advanced-Division-dart

Download Flatpak: https://sourceforge.net/projects/advanced-division/files/Advanced-Division-Kirigami-Linux.flatpak/download

Download Deb: https://sourceforge.net/projects/advanced-division/files/Advanced-Division-Kirigami-Linux.deb/download

Download archive with installation script: https://sourceforge.net/projects/advanced-division/files/Advanced-Division-Kirigami-Linux.tar.gz/download

Command for building: `cmake -B build/ . && cmake --build build/`

Learn more: https://advdiv.ratajs.cz/
